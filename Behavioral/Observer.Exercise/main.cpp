#include "stock.hpp"
#include <functional>

using namespace std;

int main()
{
	Stock misys("Misys", 340.0);
	Stock ibm("IBM", 245.0);
	Stock tpsa("TPSA", 95.0);

	// rejestracja inwestor�w zainteresowanych powiadomieniami o zmianach kursu sp�ek
	// TODO:
    Investor kulczyk {"Kulczyk"};
    Investor solorz {"Solorz"};

    misys.connect([&kulczyk](PriceChangedEventArgs args) {
        kulczyk.react(args);
    });

    auto conn = ibm.connect([&kulczyk](PriceChangedEventArgs args) {
        kulczyk.react(args);
    });

    ibm.connect(std::bind(&Investor::react, &solorz, std::placeholders::_1));

	// zmian kurs�w
	misys.set_price(360.0);
	ibm.set_price(210.0);
	tpsa.set_price(45.0);

    cout << "\n\n";

    conn.disconnect();

	misys.set_price(380.0);
	ibm.set_price(230.0);
	tpsa.set_price(15.0);
}
