#ifndef STOCK_HPP_
#define STOCK_HPP_

#include <string>
#include <iostream>
#include <boost/signals2.hpp>

struct PriceChangedEventArgs
{
    std::string symbol;
    double price;

    PriceChangedEventArgs(std::string symbol, double price) : symbol{symbol}, price{price}
    {
    }
};

using StockPriceChangedEvent = boost::signals2::signal<void (PriceChangedEventArgs)>;

using StockPriceChangedSlot = StockPriceChangedEvent::slot_type;

// Subject
class Stock
{
private:
	std::string symbol_;
	double price_;
    StockPriceChangedEvent price_changed_event_;
public:
	Stock(const std::string& symbol, double price) : symbol_(symbol), price_(price)
	{
	}

	std::string get_symbol() const
	{
		return symbol_;
	}

	double get_price() const
	{
		return price_;
	}

    boost::signals2::connection connect(StockPriceChangedSlot slot)
    {
        return price_changed_event_.connect(slot);
    }

	void set_price(double price)
	{
        if (price != price_)
        {
            price_ = price;

            // TODO: powiadomienie inwestorow o zmianie kursu...
            price_changed_event_(PriceChangedEventArgs{symbol_, price_});
        }
	}
};

//class Observer
//{
//public:
//	virtual void update(/*...*/) = 0;
//	virtual ~Observer()
//	{
//	}
//};


class Investor
{
	std::string name_;
public:
	Investor(const std::string& name) : name_(name)
	{
	}

    void react(PriceChangedEventArgs args)
    {
        std::cout << "Investor " << name_ << " notified " << " - new price of "
                  << args.symbol << " = " << args.price << std::endl;
    }
};

#endif /*STOCK_HPP_*/
