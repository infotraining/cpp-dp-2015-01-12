#ifndef STRATEGY_HPP_
#define STRATEGY_HPP_

#include <iostream>
#include <memory>

// "Strategy"
class Strategy
{	
public:
	virtual void algorithm_interface() = 0;
    virtual ~Strategy() = default;
};

// "ConcreteStrategyA"
class ConcreteStrategyA : public Strategy
{
public:
	virtual void algorithm_interface()
	{
		std::cout << "Called ConcreteStrategyA.algorithm_interface()\n";
	}
};

// "ConcreteStrategyB"
class ConcreteStrategyB : public Strategy
{
public:
	virtual void algorithm_interface()
	{
		std::cout << "Called ConcreteStrategyB.algorithm_interface()\n";
	}
};

// "ConcreteStrategyC"
class ConcreteStrategyC : public Strategy
{
public:
	virtual void algorithm_interface()
	{
		std::cout << "Called ConcreteStrategyC.algorithm_interface()\n";
	}
};

// "Context" 
class Context
{
    std::shared_ptr<Strategy> strategy_;

public:
    Context(std::shared_ptr<Strategy> strategy) : strategy_ {strategy}
    {
    }
	
    void reset_strategy(std::shared_ptr<Strategy> new_strategy)
	{
        strategy_ = new_strategy;
	}

    void context_interface()
    {
    	strategy_->algorithm_interface();
    }   
};

namespace Alt
{
    class Context
    {
        std::function<void()> strategy_;

    public:
        Context(std::function<void()> strategy) : strategy_ {strategy}
        {
        }

        void reset_strategy(std::function<void()> new_strategy)
        {
            strategy_ = new_strategy;
        }

        void context_interface()
        {
            strategy_();
        }
    };

    class AltStrategyA
    {
    public:
        void operator()()
        {
            std::cout << "AltStategyA()" << std::endl;
        }
    };

    class AltStrategyB
    {
    public:
        void operator()()
        {
            std::cout << "AltStategyB()" << std::endl;
        }
    };
}

#endif /*STRATEGY_HPP_*/
