#include <iostream>
#include "singleton.hpp"

using namespace std;

int main()
{
    MagicStatic::Singleton::instance().do_something();

    MagicStatic::Singleton& singleObject = MagicStatic::Singleton::instance();
	singleObject.do_something();
}
