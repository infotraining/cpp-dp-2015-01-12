#ifndef SQUARE_HPP_
#define SQUARE_HPP_

// TODO: Dodać klase Square

#include "shape.hpp"

namespace Drawing
{
    class Square : public ShapeBase
    {
        int size_;
    public:
        Square(int x  = 0, int y = 0, int size = 0) : ShapeBase {x, y}, size_{size}
        {}

        void draw() const
        {
            std::cout << "Drawing square at " << point() << " with size: " << size_ << std::endl;
        }

        void set_size(int size)
        {
            size_ = size;
        }

        void read(std::istream& in)
        {
            Point pt;
            int size;

            in >>  pt >> size;

            set_point(pt);
            set_size(size);
        }

        void write(std::ostream& out)
        {
            out << "Square " << point() << " " << size_ << std::endl;
        }
    };
}

#endif /* SQUARE_HPP_ */
